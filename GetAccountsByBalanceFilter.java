package nxt.addons;

import nxt.Nxt;
import nxt.NxtException;
import nxt.account.Account;
import static nxt.http.ParameterParser.getChain;

import nxt.blockchain.Block;
import nxt.blockchain.Chain;
import nxt.db.DbUtils;
import nxt.dbschema.Db;
import nxt.http.APIServlet;
import nxt.http.APITag;
import nxt.util.Convert;

import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.lang.Math;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;

public final class GetAccountsByBalanceFilter implements AddOn {

    public APIServlet.APIRequestHandler getAPIRequestHandler() {
        return new GetAccountsByBalanceFilterAPI(new APITag[]{APITag.ADDONS}, "maximumNQT", "minimumNQT", "limit");
    }

    public String getAPIRequestType() {
        return "getAccountsByBalanceFilter";
    }

    public static class GetAccountsByBalanceFilterAPI extends APIServlet.APIRequestHandler {

        private GetAccountsByBalanceFilterAPI(APITag[] apiTags, String... origParameters) {
            super(apiTags, origParameters);
        }

        @Override
        protected JSONStreamAware processRequest(HttpServletRequest req) throws NxtException {
            Chain chain = getChain(req, true);

            String balanceMaximum = Convert.emptyToNull(req.getParameter("maximumNQT"));
            String balanceMinimum = Convert.emptyToNull(req.getParameter("minimumNQT"));
            String accountLimit = Convert.emptyToNull(req.getParameter("limit"));

            long varBalanceMaximum = -1;
            long varBalanceMinimum = -1;
            int varAccountLimit = -1;

            if (accountLimit != null)
                varAccountLimit = Integer.parseInt(accountLimit);

            if (balanceMaximum != null)
                varBalanceMaximum = Long.parseLong(balanceMaximum);

            if (balanceMinimum != null)
                varBalanceMinimum = Long.parseLong(balanceMinimum);

            if (varBalanceMaximum >= 0 && varBalanceMaximum < varBalanceMinimum)
                varBalanceMaximum = varBalanceMinimum;

            Nxt.getBlockchain().readLock();
            Block blockData = Nxt.getBlockchain().getLastBlock();
            LinkedHashMap<Long, Long> map = getAccountsByBalanceFilter(chain, varBalanceMaximum, varBalanceMinimum, varAccountLimit);
            Nxt.getBlockchain().readUnlock();

            final double floatingPoinmtAdjustment = Math.pow(10, chain.getDecimals());

            JSONArray jsonArray = new JSONArray();

            long balanceNQTTotal = 0;
            int numberOfAccounts = 0; //map.size();

            for (Map.Entry<Long, Long> entry : map.entrySet()) {
                JSONObject json = new JSONObject();

                long accountId = entry.getKey();
                long accountBalance = entry.getValue();

                json.put("account", Long.toUnsignedString(accountId));
                json.put("accountRS", Convert.rsAccount(accountId));
                json.put("balanceNQT", Long.toUnsignedString(accountBalance));
                json.put("balanceFloat", accountBalance / floatingPoinmtAdjustment);

                byte[] publicKey = Account.getPublicKey(accountId);

                if (publicKey != null) {
                    json.put("publicKey", Convert.toHexString(publicKey));
                }

                numberOfAccounts++;
                balanceNQTTotal += accountBalance;
                jsonArray.add(json);
            }

            JSONObject response = new JSONObject();

            response.put("block", Long.toUnsignedString(blockData.getId()));
            response.put("height", blockData.getHeight());

            response.put("chain", chain.getName());
            response.put("count", numberOfAccounts);
            response.put("balanceTotalNQT", Long.toUnsignedString(balanceNQTTotal));
            response.put("balanceTotalFloat", balanceNQTTotal / floatingPoinmtAdjustment);

            if (varBalanceMaximum > 0)
                response.put("maximumNQT", Long.toUnsignedString(varBalanceMaximum));

            if (varBalanceMinimum > 0)
                response.put("minimumNQT", Long.toUnsignedString(varBalanceMinimum));

            response.put("accounts", jsonArray);

            return response;
        }

        public static LinkedHashMap<Long, Long> getAccountsByBalanceFilter(Chain chain, final long balanceMaximum, final long balanceMinimum, final int limit) {
            Connection con = null;
            LinkedHashMap<Long, Long> map = new LinkedHashMap<>();
            try {
                StringBuilder sb = new StringBuilder();

                con = Db.getConnection();

                sb.append("SELECT account_id, balance FROM ");

                if (chain.getId() == 1) {
                    sb.append("public.balance_fxt");
                } else {
                    sb.append(chain.getName());
                    sb.append(".balance");
                }

                sb.append(" WHERE latest = TRUE");

                if (balanceMinimum >= 0) {
                    sb.append(" AND balance >= ");
                    sb.append(balanceMinimum);
                }

                if(balanceMaximum >= 0) {
                    sb.append(" AND balance <= ");
                    sb.append(balanceMaximum);
                }

                sb.append(" ORDER BY balance desc");

                if(limit > 0)
                    sb.append(" limit " + limit);

                PreparedStatement pstmt = con.prepareStatement(sb.toString());
                int i = 0;

                ResultSet rs = pstmt.executeQuery();

                while (rs.next()) {
                    long accountId = rs.getLong("account_id");
                    long balance = rs.getLong("balance");

                    map.put(accountId, balance);
                };

                con.close();
                return map;

            } catch (SQLException e) {
                DbUtils.close(con);
                throw new RuntimeException(e.toString(), e);
            }
        }

        @Override
        protected boolean isChainSpecific() {
            return true;
        }
    }
}
